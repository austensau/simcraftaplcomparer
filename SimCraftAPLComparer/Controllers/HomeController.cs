﻿using Microsoft.AspNetCore.Mvc;
using SimCraftAPLComparer.Models;
using System.Diagnostics;

namespace SimCraftAPLComparer.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Events()
        {
            return View(CombatLog.EncounterEvents.Where(i =>
            {
                if (i.SubEvent.Suffix == Suffix.CAST_SUCCESS)
                    return true;
                return false;
            }).Take(200));
        }

        public IActionResult States()
        {
            return View(PlayerStates.States.Take(1000));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}