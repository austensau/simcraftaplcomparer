﻿namespace SimCraftAPLComparer.Models
{
    public class SubEvent
    {
        //TODO: Class makes use of Enum.TryParse which is slow. Consider using TryParse to generate a dictionary to speed up results.

        ///// <summary>
        ///// Possible SubEvent Prefixes.
        ///// </summary>
        //public static readonly Dictionary<string, int> Prefixes = new()
        //{
        //    ["SWING"] = 1, ["RANGE"] = 2, ["SPELL"] = 3, ["SPELL_PERIODIC"] = 4,
        //    ["SPELL_BUILDING"] = 5, ["ENVIRONMENTAL"] = 6
        //    };
        ///// <summary>
        ///// Possible SubEvent Suffixes
        ///// </summary>
        //public static readonly Dictionary<string, int> Suffixes = new()
        //{
        //    ["DAMAGE"] = 1, ["MISSED"] = 2, ["HEAL"] = 3, ["ENERGIZE"] = 4, ["DRAIN"] = 5, ["LEECH"] = 6, 
        //    ["INTERRUPT"] = 7, ["DISPEL"] = 8, ["DISPEL_FAILED"] = 9, ["STOLEN"] = 10, ["EXTRA_ATTACKS"] = 11,
        //    ["AURA_APPLIED"] = 12, ["AURA_REMOVED"] = 13, ["AURA_APPLIED_DOSE"] = 14, ["AURA_REMOVED_DOSE"] = 15,
        //    ["AURA_REFRESH"] = 16, ["AURA_BROKEN"] = 17, ["AURA_BROKEN_SPELL"] = 18, ["CAST_START"] = 19,
        //    ["CAST_SUCCESS"] = 20, ["CAST_FAILED"] = 21, ["INSTAKILL"] = 22, ["DURABILITY_DAMAGE"] = 23,
        //    ["DURABILITY_DAMAGE_ALL"] = 24, ["CREATE"] = 25, ["SUMMON"] = 26, ["RESURRECT"] = 27};
        ///// <summary>
        ///// Special SubEvents
        ///// </summary>
        //public static readonly Dictionary<string, int> Specials = new()
        //{
        //    ["DAMAGE_SPLIT"] = 1, ["DAMAGE_SHIELD"] = 2, ["DAMAGE_SHIELD_MISSED"] = 3,
        //    ["ENCHANT_APPLIED"] = 4, ["ENCHANT_REMOVED"] = 5, ["PARTY_KILL"] = 6,
        //    ["UNIT_DIED"] = 7, ["UNIT_DESTROYED"] = 8, ["UNIT_DISSIPATES"] = 9,
        //    ["ENCOUNTER_START"] = 10, ["ENCOUNTER_END"] = 11
        //};

        /// <summary>
        /// SubEvent full type - Prefix_Suffix or Special if not NONE
        /// </summary>
        public string Type { get
            {
                if (Special == Special.NONE)
                {
                    return Prefix + "_" + Suffix;
                }
                return Special.ToString();
            }
        }
        public Prefix Prefix { get; private set; }
        public Suffix Suffix { get; private set; }
        public Special Special { get; private set; }
        public SubEvent(string subevent)
        {
            var components = subevent.Split('_');
            Special = ConfigureSpecial(subevent);
            if(Special == Special.NONE)
            {
                (Prefix, Suffix) = FindSplit(components);
            }
        }

        /// <summary>
        /// Split the subevent into its prefix and suffix.
        /// </summary>
        /// <param name="components"></param>
        /// <param name="prefixes"></param>
        /// <param name="suffixes"></param>
        /// <returns>A tuple of the Prefix and Suffix values.</returns>
        private (Prefix, Suffix) FindSplit(string[] components)
        {
            var enums = (Prefix.NONE, Suffix.NONE);
            for(int i = 0; i <= components.Length - 2; i++)
            {
                var prefix_split = "";
                var suffix_split = "";
                for (int j = 0; j <= i; j++)
                {
                    prefix_split += "_" + components[j];
                }
                if (!Enum.TryParse(prefix_split[1..], out Prefix pre_val)) continue;
                for(int j = i + 1; j <= components.Length - 1; j++)
                {
                    suffix_split += "_" + components[j];
                }
                if(Enum.TryParse(suffix_split[1..], out Suffix suf_val))
                {
                    return enums = (pre_val, suf_val);
                }
            }
            return enums;
        }
        /// <summary>
        /// Returns the Special subevent if it exists and configures Prefix and Suffix.
        /// </summary>
        /// <param name="subevent"></param>
        /// <returns></returns>
        private Special ConfigureSpecial(string subevent)
        {
            _ = Enum.TryParse(subevent, out Special value);
            if (value == Special.DAMAGE_SPLIT ||
                value == Special.DAMAGE_SHIELD ||
                value == Special.DAMAGE_SHIELD_MISSED)
            {
                Prefix = Prefix.SPELL;
                Suffix = value == Special.DAMAGE_SHIELD_MISSED ? Suffix.MISSED : Suffix.DAMAGE;
            }
            return value;
        }
    }

    public enum Prefix
    {
        NONE,
        SWING,
        RANGE,
        SPELL,
        SPELL_PERIODIC,
        SPELL_BUILDING,
        ENVIRONMENTAL
    }

    public enum Suffix
    {
        NONE,
        DAMAGE, MISSED, HEAL, ENERGIZE, DRAIN, LEECH, INTERRUPT, DISPEL, DISPEL_FAILED, STOLEN,
        EXTRA_ATTACKS, AURA_APPLIED, AURA_REMOVED, AURA_APPLIED_DOSE, AURA_REMOVED_DOSE, AURA_REFRESH, AURA_BROKEN,
        AURA_BROKEN_SPELL, CAST_START, CAST_SUCCESS, CAST_FAILED, INSTAKILL, DURABILITY_DAMAGE, DURABILITY_DAMAGE_ALL, CREATE,
        SUMMON, RESURRECT
    }

    public enum Special
    {
        NONE,
        DAMAGE_SPLIT,
        DAMAGE_SHIELD,
        DAMAGE_SHIELD_MISSED,
        ENCHANT_APPLIED,
        ENCHANT_REMOVED,
        PARTY_KILL,
        UNIT_DIED,
        UNIT_DESTROYED,
        UNIT_DISSIPATES,
        ENCOUNTER_START,
        ENCOUNTER_END
    }
}
