﻿namespace SimCraftAPLComparer.Models
{
    public class Aura
    {
        public string SpellId { get; }
        public string SpellName { get; }
        public int StackCount { get; set; }
        public string TooltipUrl { get { return $"https://www.wowhead.com/spell={SpellId}"; } } 

        public Aura (string id, string name, int count)
        {
            SpellId = id;
            SpellName = name;
            StackCount = count;
        }

        public override string ToString()
        {
            return SpellName + (StackCount == 1 ? "" : "(" + StackCount + ")");
        }

    }
}
