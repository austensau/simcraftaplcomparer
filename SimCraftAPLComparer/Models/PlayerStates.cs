﻿namespace SimCraftAPLComparer.Models
{
    public class PlayerStates
    {
        private static readonly List<Player> states = new();
        private static Player prevState = new(new CombatEvent(), "Spelmasta-Hyjal");

        public static IEnumerable<Player> States
        {
            get
            {
                foreach (var @event in CombatLog.EncounterEvents)
                {
                    if (@event.SubEvent.Suffix == Suffix.CAST_SUCCESS)
                    {
                        var newState = new Player(prevState, @event);
                        prevState = newState;
                        //yield return newState;
                        states.Add(newState);
                    }
                    else
                        prevState.ProcessAuras(@event);
                }
                return states;
            }
        }

    }
}
