﻿namespace SimCraftAPLComparer.Models
{
    public interface IAdvancedEvent : ICombatEvent
    {
        public string InfoGUID { get; }                        // 13
        public string OwnerGUID { get; }                       // 14
        public int CurrentHP { get; }                          // 15
        public int MaxHP { get; }                              // 16
        public int AttackPower { get; }                        // 17
        public int SpellPower { get; }                         // 18
        public int Armor { get; }                              // 19
        public int Absorb { get; }                             // 20
        public int PowerType { get; }                          // 21
        public int CurrentPower { get; }                       // 22
        public int MaxPower { get; }                           // 23
        public int PowerCost { get; }                          // 24
        public double PositionX { get; }                       // 25
        public double PositionY { get; }                       // 26
        public int UiMapID { get; }                            // 27
        public double Facing { get; }                          // 28
        public int Level { get; }                              // 29
    }
}
