﻿namespace SimCraftAPLComparer.Models
{
    public static class CombatLog
    {
        public static bool AdvancedLogging { get; private set; } = false;
        private const string folder = @"D:\BattleNet\World of Warcraft\_retail_\Logs\";
        private const string file = "WoWCombatLog-120321_175233.txt";
        private const string sourceName = "Spelmasta-Hyjal";
        private const int maxLoad = 1000000;
        public static IEnumerable<CombatEvent> EncounterEvents
        {
            get
            {
                var fileLines = File.ReadLines(path: folder + file);
                bool encounterInProgress = false;
                if (fileLines.First().Contains("ADVANCED_LOG_ENABLED,1")) AdvancedLogging = true;
                foreach (string line in fileLines)
                {
                    var @event = new CombatEvent(line);
                    if (@event.SubEvent.Special == Special.ENCOUNTER_START) encounterInProgress = true;
                    if (@event.SubEvent.Special == Special.ENCOUNTER_END) encounterInProgress = false;
                    if (encounterInProgress || !(@event.SubEvent.Suffix == Suffix.CAST_SUCCESS))
                    {
                        int count = 0;
                        if (@event.SourceName == sourceName)
                        {
                            yield return @event;
                            count++;
                        }
                        if (count > maxLoad && maxLoad != 0)
                        {
                            yield break;
                        }
                    }
                }
                yield break;
            }
        }
    }
}
