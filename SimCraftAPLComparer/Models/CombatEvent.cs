﻿using System;
using System.Collections.Generic;

namespace SimCraftAPLComparer.Models
{
    /// <summary>
    /// Class representing a combat log event containing up to 39 parameters.
    /// Parameters include 9 base, 0 - 3 prefix, 0 or 17 advanced, 0 - 10 suffix not counting timestamp.
    /// </summary>
    public class CombatEvent : ICombatEvent, IAdvancedEvent
    {
        public static bool AdvancedLogging { get; private set; } = false;

        //Base member implementations of ICombatEvent
        #region BaseParameters
        public string RawEvent { get; } = string.Empty;
        //Parameters 1 - 9 are the base parameters of a Combat Event
        //https://wowpedia.fandom.com/wiki/COMBAT_LOG_EVENT#Base_Parameters
        public static DateTime? ReferenceTIme { get; private set; } = null;
        public TimeSpan TimeStamp { get; }                      // 0
        public SubEvent SubEvent { get; }                       // 1
        public string SourceGUID { get; } = string.Empty;       // 2
        public string SourceName { get; } = string.Empty;       // 3
        public string SourceFlags { get; } = string.Empty;      // 4
        public string SourceRaidFlags { get; } = string.Empty;  // 5
        public string DestGUID { get; } = string.Empty;         // 6
        public string DestName { get; } = string.Empty;         // 7
        public string DestFlags { get; } = string.Empty;        // 8
        public string DestRaidFlags { get; } = string.Empty;    // 9
        #endregion

        //Advanced member implementations of IAdvancedEvent
        #region AdvancedParameters
        //parameters 13 - 29 (17 total) are advanced combat log parameters
        public string InfoGUID { get; } = string.Empty;             // 13
        public string OwnerGUID { get; } = string.Empty;            // 14
        public int CurrentHP { get; } = 0;                          // 15
        public int MaxHP { get; } = 0;                              // 16
        public int AttackPower { get; } = 0;                        // 17
        public int SpellPower { get; } = 0;                         // 18
        public int Armor { get; } = 0;                              // 19
        public int Absorb { get; } = 0;                             // 20
        public int PowerType { get; } = 0;                          // 21
        public int CurrentPower { get; } = 0;                       // 22
        public int MaxPower { get; } = 0;                           // 23
        public int PowerCost { get; } = 0;                          // 24
        public double PositionX { get; } = 0;                       // 25
        public double PositionY { get; } = 0;                       // 26
        public int UiMapID { get; } = 0;                            // 27
        public double Facing { get; } = 0;                          // 28
        public int Level { get; } = 0;                              // 29
        #endregion

        
        //parameters 10 - 12 are optional parameters for prefixes - RANGE, SPELL, SPELL_PERIODIC, SPELL_BUILDING
        public string SpellId { get; } = string.Empty;          // 10
        public string SpellName { get; } = string.Empty;        // 11
        public string SpellSchool { get; } = string.Empty;      // 12
        

        //Parameters 30 - 39 (10 total) are parameters for suffixes - ...
        public string AuraType { get; } = string.Empty;    // 13 - Aura Applied / Removed only
        public int AuraAmount { get; } = 0;                 // 14 - Dose only
        public string TooltipUrl { get { return $"https://www.wowhead.com/spell={SpellId}"; } }

        public CombatEvent() { }

        public CombatEvent(string @event)
        {
            string[] dateTimeSplit = @event.Split(new char[] { ' ' }, 4);
            string[] eventSplit = dateTimeSplit[3].Split(',');
            int suffixIndexOffset = eventSplit.Length >= 26 ? 17 : 0; //indicates 17 advanced parameters are included in event params
            RawEvent = @event;
            TimeStamp = CreateDateTime(dateTimeSplit);
            SubEvent = new SubEvent(eventSplit[0]);
            if (SubEvent.Suffix == Suffix.CAST_SUCCESS || SubEvent.Suffix == Suffix.AURA_APPLIED ||
                SubEvent.Suffix == Suffix.AURA_APPLIED_DOSE || SubEvent.Suffix == Suffix.AURA_REMOVED || SubEvent.Suffix == Suffix.AURA_REMOVED_DOSE)
            {
                SourceGUID = eventSplit[1];
                SourceName = eventSplit[2].Replace("\"", string.Empty);
                SourceFlags = eventSplit[3];
                SourceRaidFlags = eventSplit[4];
                DestGUID = eventSplit[5];
                DestName = eventSplit[6] == "nil" ? "NA" : eventSplit[6].Replace("\"", string.Empty);
                DestFlags = eventSplit[7];
                DestRaidFlags = eventSplit[8];
                SpellId = eventSplit[9];
                SpellName = eventSplit[10].Replace("\"", string.Empty);
                SpellSchool = eventSplit[11];
                if(SubEvent.Type.Contains("AURA"))
                    AuraType = eventSplit[12 + suffixIndexOffset];
                if (SubEvent.Type.Contains("DOSE"))
                    AuraAmount = int.Parse(eventSplit[13 + suffixIndexOffset]);
            }
        }

        public TimeSpan CreateDateTime(string[] dateSplit)
        {
            int year = DateTime.Today.Year;
            int month = int.Parse(dateSplit[0].Split('/')[0]);
            int day = int.Parse(dateSplit[0].Split('/')[1]);
            int hour = int.Parse(dateSplit[1].Split(':')[0]);
            int minute = int.Parse(dateSplit[1].Split(':')[1]);
            int second = int.Parse(dateSplit[1].Split(':')[2].Split('.')[0]);
            int millisecond = int.Parse(dateSplit[1].Split(':')[2].Split('.')[1]);
            var dt = new DateTime(year, month, day, hour, minute, second, millisecond);
            if (ReferenceTIme is null)
            {
                ReferenceTIme = dt;
            }
            return (TimeSpan)(dt - ReferenceTIme);
        }

        public override string ToString()
        {
            return SourceName + " " + SubEvent + " " + SpellName + (DestName == "nil" ? "" : "on " + DestName);
        }

    }
}
