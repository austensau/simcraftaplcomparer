﻿namespace SimCraftAPLComparer.Models
{
    public class Player
    {
        public string Name { get; private set; }
        public string Guid { get; private set; }
        public List<Aura> Buffs { get; private set; }
        public CombatEvent LastAction { get; set; }

        public Player(CombatEvent lastAction, string name, string guid = "0")
        {
            Name = name;
            Guid = guid;
            Buffs = new List<Aura>();
            LastAction = lastAction;
        }

        public Player(CombatEvent lastAction)
        {
            Buffs = new List<Aura>();
            Name = lastAction.SourceName;
            Guid = lastAction.SourceGUID;
            LastAction = lastAction;
        }

        public Player(Player lastState, CombatEvent lastAction)
        {
            Buffs = new List<Aura>(lastState.Buffs);
            Name = lastState.Name;
            Guid = lastState.Guid;
            LastAction = lastAction;
        }

        public void ProcessAuras(CombatEvent action)
        {
            if (action.DestName != action.SourceName) return;
            if(action.AuraType != "BUFF") return;
            if (action.SubEvent.Suffix == Suffix.AURA_APPLIED)
            {
                Buffs.Add(new Aura(action.SpellId, action.SpellName, 1));
            }
            else if (action.SubEvent.Suffix == Suffix.AURA_APPLIED_DOSE ||
                action.SubEvent.Suffix == Suffix.AURA_REMOVED_DOSE)
            {
                Aura? buff = Buffs.Find(x => x.SpellName == action.SpellName);
                if (buff is not null) {
                    buff.StackCount = action.AuraAmount;
                    if(buff.StackCount == 0)
                    {
                        Buffs.Remove(buff);
                    }
                }
            }
            else if (action.SubEvent.Suffix == Suffix.AURA_REMOVED)
            {
                //! annotation added because Find should never fail.
                if(Buffs.Remove(Buffs.Find(x => x.SpellName == action.SpellName)!) == false)
                {
                    //throw new Exception("Failed to rmeove an existing buff.");
                }
            }
        }

        public string GetPlayerAuras()
        {
            if (this.Buffs.Count == 0) return "";
            string buffs = "";
            for(int i = 0; i < Buffs.Count; i++)
            {
                buffs = Buffs[i] + ", " + buffs;
            }
            return buffs;
        }
    }
}
