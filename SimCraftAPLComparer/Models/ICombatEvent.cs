﻿namespace SimCraftAPLComparer.Models
{
    public interface ICombatEvent
    {
        public string RawEvent { get; }

        //Parameters 1 - 9 are the base parameters
        //https://wowpedia.fandom.com/wiki/COMBAT_LOG_EVENT#Base_Parameters
        public static DateTime? ReferenceTIme { get; private set; } = null;
        public TimeSpan TimeStamp { get; }                      // 0
        public SubEvent SubEvent { get; }         // 1
        public string SourceGUID { get; }       // 2
        public string SourceName { get; }       // 3
        public string SourceFlags { get; }      // 4
        public string SourceRaidFlags { get; }  // 5
        public string DestGUID { get; }         // 6
        public string DestName { get; }         // 7
        public string DestFlags { get; }        // 8
        public string DestRaidFlags { get; }    // 9
    }

}
